import * as WebBrowser from 'expo-web-browser';
import React from 'react';
import { StyleSheet, Image, Text, View } from 'react-native';
import Touchable from 'react-native-platform-touchable';
import { Ionicons } from '@expo/vector-icons';

export default class ExpoLinksScreen1 extends React.Component {
  render() {
    return (
      <View>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressDocs}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
            <Ionicons name="ios-easel" size={22} color="#ccc" />

            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>Historia</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressDocs}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
            <Ionicons name="ios-easel" size={22} color="#ccc" />
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>Especificaciones</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressDocs}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
            <Ionicons name="ios-easel" size={22} color="#ccc" />
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>Manual de mantenimientooo</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressDocs}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
            <Ionicons name="ios-easel" size={22} color="#ccc" />
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>IPC</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressDocs}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Ionicons name="ios-easel" size={22} color="#ccc" />
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>Fotos del Falcon</Text>
            </View>
          </View>
        </Touchable>
        <Text style={styles.optionsTitleText}>ATAs</Text>

        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressDocs}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
            <Ionicons name="ios-checkmark-circle" size={22} color="#ccc" />

            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>ATA 32 Tren de aterrizaje</Text>
            </View>
          </View>
        </Touchable>

        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressForums}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Ionicons name="ios-checkmark-circle" size={22} color="#ccc" />
              
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>33 Luces</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressForums}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Ionicons name="ios-checkmark-circle" size={22} color="#ccc" />
              
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>52 Puertas</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressForums}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Ionicons name="ios-checkmark-circle" size={22} color="#ccc" />
              
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>53 Fuselaje</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressForums}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Ionicons name="ios-checkmark-circle" size={22} color="#ccc" />
              
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>54 pilones y barquillos</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressForums}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Ionicons name="ios-checkmark-circle" size={22} color="#ccc" />
              
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>55 Estabilizadores</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressForums}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Ionicons name="ios-checkmark-circle" size={22} color="#ccc" />
              
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>56 Ventanas</Text>
            </View>
          </View>
        </Touchable>
        <Touchable
          style={styles.option}
          background={Touchable.Ripple('#ccc', false)}
          onPress={this._handlePressForums}>
          <View style={{ flexDirection: 'row' }}>
            <View style={styles.optionIconContainer}>
              <Ionicons name="ios-checkmark-circle" size={22} color="#ccc" />
              
            </View>
            <View style={styles.optionTextContainer}>
              <Text style={styles.optionText}>57 Alas</Text>
            </View>
          </View>
        </Touchable>
        

      </View>
    );
  }

  _handlePressDocs = () => {
    WebBrowser.openBrowserAsync('http://docs.expo.io');
  };

  _handlePressForums = () => {
    WebBrowser.openBrowserAsync('http://forums.expo.io');
  };
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    paddingTop: 15,
  },
  optionsTitleText: {
    fontSize: 16,
    marginLeft: 15,
    marginTop: 9,
    marginBottom: 12,
  },
  optionIconContainer: {
    marginRight: 9,
  },
  option: {
    backgroundColor: '#fdfdfd',
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderBottomWidth: StyleSheet.hairlineWidth,
    borderBottomColor: '#EDEDED',
  },
  optionText: {
    fontSize: 15,
    marginTop: 1,
  },
});
